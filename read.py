#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import time
from Tkinter import *
mem=str(os.popen('free -t -m').readlines())
card=str(os.popen('nvidia-smi --query-gpu=name --format=csv,noheader').readlines())
cardstr=str(os.popen('nvidia-settings -q [gpu:0]/GPUCurrentClockFreqsString').readlines())
cpufreq=str(os.popen('cat /proc/cpuinfo | grep -i mhz').readlines())
ncpus = os.sysconf("SC_NPROCESSORS_ONLN")
cardtemp=str(os.popen('nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader').readlines())
cputemp = str(os.popen("/usr/bin/sensors | grep Package\ id\ 0").readlines())
cpuname = str(os.popen("cat /proc/cpuinfo | grep 'model name' | head -n 1").readlines())

"""
Get a whole line of memory output, it will be something like below
['             total       used       free     shared    buffers     cached\n', 
'Mem:           925        591        334         14         30        355\n', 
'-/+ buffers/cache:        205        719\n', 
'Swap:           99          0         99\n', 
'Total:        1025        591        434\n']
 So, we need total memory, usage and free memory.
 We should find the index of capital T which is unique at this string
"""
T_ind=mem.index('T')
"""
Than, we can recreate the string with this information. After T we have,
"Total:        " which has 14 characters, so we can start from index of T +14
and last 4 characters are also not necessary.
We can create a new sub-string using this information
"""
mem_G=mem[T_ind+14:-4]
"""
The result will be like
1025        603        422
we need to find first index of the first space, and we can start our substring
from from 0 to this index number, this will give us the string of total memory
"""
S1_ind=mem_G.index(' ')
mem_T=mem_G[0:S1_ind]
"""
Similarly we will create a new sub-string, which will start at the second value. 
The resulting string will be like
603        422
Again, we should find the index of first space and than the 
take the Used Memory and Free memory.
"""
mem_G1=mem_G[S1_ind+8:]
S2_ind=mem_G1.index(' ')
mem_U=mem_G1[0:S2_ind]

mem_F=mem_G1[S2_ind+8:]

card=card[2:-4]

list = cardstr.split(" ")

cardfreq = list[6]
cardfreq = cardfreq[8:-5]
cardmem = list[18]
cardmem = cardmem[16:-5]
cardtemp = cardtemp[2:-4]

cpufreqlist = cpufreq.split(" ")
cpufreq = cpufreqlist[20]
cpufreq = cpufreq[0:-8]

ncpus = str(ncpus)
cputemplist = cputemp.split("+")
cputempshort = cputemplist[1]
cputempshortlist = cputempshort.split(".")
cputemp = cputempshortlist[0]

cpuname= cpuname[16:-4]

print 'Summary = ' + mem_G
print 'Total Memory = ' + mem_T +' MB'
print 'Used Memory = ' + mem_U +' MB'
print 'Free Memory = ' + mem_F +' MB'
print 'Card = ' + card
print 'CardFreq = ' + cardfreq + ' Mhz'
print 'Memoryfreq = ' + cardmem  + ' Mhz'
print 'Cpu Frequency = ' + cpufreq + 'Mhz'
print 'Number of Threads = ' + ncpus + ' Cores'
print 'Card temperature = ' + cardtemp + ' Degrees'
print 'Cputemp = ' + cputemp + ' Degrees'
print cpuname

root = Tk()

video = Label(root, text="Gpu information", bg="green", fg="white")
video.pack()
spacer_0 = Label(root, text="\n", height=1)
spacer_0.pack()
cardLabel = Label(root, text= "Videocard name: " + card, fg="white")
cardLabel.pack()
cardfreqLabel= Label(root, text= "Videocard frequency: " + cardfreq + "Mhz", fg="white")
cardfreqLabel.pack()
cardmemLabel= Label(root, text= "Videocard memoryfrequency: " + cardmem + "Mhz", fg="white")
cardmemLabel.pack()
cardtempLabel= Label(root, text= "Videocard temprature: " + cardtemp + "°C", fg="white")
cardtempLabel.pack()
spacer_1 = Label(root, text="\n", height=1)
spacer_1.pack()

cpu = Label(root, text="Cpu information", bg="blue", fg="white")
cpu.pack()
spacer_2 = Label(root, text="\n", height=1)
spacer_2.pack()
cpunameLabel = Label(root, text= "Processor name: " + cpuname , fg="white")
cpunameLabel.pack()
cpufreqLabel = Label(root, text= "Processor frequency: " + cpufreq + "Mhz" , fg="white")
cpufreqLabel.pack()
cpuncpusLabel = Label(root, text= "Number of Threads: " + ncpus + " Threads" , fg="white")
cpuncpusLabel.pack()
cputempLabel = Label(root, text= "Cpu temperature: " + cputemp + "°C" , fg="white")
cputempLabel.pack()
spacer_3 = Label(root, text="\n", height=1)
spacer_3.pack()

memLabel = Label(root, text= "Memory details: ", bg='grey', fg='white')
memLabel.pack()
spacer_4 = Label(root, text="\n", height=1)
spacer_4.pack()
cpunameLabel = Label(root, text= "Total memory amount: " + mem_T + 'MB' , fg="white")
cpunameLabel.pack()
cpunameLabel = Label(root, text= "Used memory amount: " + mem_U  + 'MB', fg="white")
cpunameLabel.pack()
cpunameLabel = Label(root, text= "Free memory amount: " + mem_F + 'MB' , fg="white")
cpunameLabel.pack()


root.mainloop() 

root.update()		